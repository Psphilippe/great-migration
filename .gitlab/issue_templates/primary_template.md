## Prework: 

Please follow steps in https://gitlab.com/gitlab-data/great-migration#how-to-contribute

This issue is from Airtable row: 

[If (d)]
* [ ]  This issue is part of dashboard: 
* [ ]  This is a single visualization dashboard

## Existing (generated) SQL
<details>
<summary> SQL statement </summary>

<pre><code>

Paste the generated SQL here, including the command.

</code></pre>
</details>


## Screenshot
(attachment)

## CSV download
(attachment)