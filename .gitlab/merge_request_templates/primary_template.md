## Issue

Link the Issue this MR closes

**Issue Checklist**
Issue has/is:
* [ ]  existing (generated) SQL
* [ ]  screenshot of visualization
* [ ]  CSV download
* [ ]  confidential, if appropriate

## Solution

This MR adds the SQL for this query to the project. 
* [ ]  The SQL matches the Data Team style guide.
* [ ]  Results have been verified against screenshots + the CSV output in the issue.
* [ ]  The file location matches the issue-hierarchy.
* [ ]  Follows the naming convention of `analysis type, data source (in alpha order, if multiple), thing, aggregation` (e.g. `retention_sfdc_zuora_customer_count.sql`)

### Related Links

* [ ]  Mode:
* [ ]  Periscope:
* [ ]  Redash: 
* [ ]  Other:

## All MRs Checklist
* [ ]  This MR is ready for final review and merge.
* [ ]  Assigned to reviewer

## Reviewer Checklist
* [ ]  Check before setting to merge

## Further changes requested
* [ ]  AUTHOR: Uncheck all boxes before taking further action. 
