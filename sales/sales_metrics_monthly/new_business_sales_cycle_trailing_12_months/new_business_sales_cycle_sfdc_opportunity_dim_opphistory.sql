WITH opp_sfdc AS (

  SELECT *
  FROM ANALYTICS.SFDC_OPPORTUNITY_XF

), dim_sfdc AS  (

  SELECT *
  FROM DIM_OPPHISTORY

)

SELECT AVG(dim_sfdc.DAYS_IN_STAGE) AS days,
          opp_sfdc.DEAL_SIZE,
          opp_sfdc.STAGE_NAME
FROM opp_sfdc
LEFT JOIN dim_sfdc
  ON opp_sfdc.opportunity_id = dim_sfdc.opportunityid
WHERE opp_sfdc.stage_name IN ('0-Pending Acceptance', '1-Discovery', '2-Scoping', '3-Technical Evaluation', '4-Proposal', '5-Negotiating', '6-Awaiting Signature')
  AND (opp_sfdc.opportunity_owner_tags LIKE '%Account Manager%'
  OR opp_sfdc.opportunity_owner_tags LIKE '%Account Executive%')
  AND (opp_sfdc.sales_type = 'New Business')
GROUP BY 2, 3
ORDER BY 2, 3