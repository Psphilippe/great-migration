with base as (

        SELECT * FROM analytics.sfdc_opportunity_xf
)

SELECT close_date::date as close_date,
        is_risky, 
        opportunity_name,
        sales_type,
        OPPORTUNITY_OWNER,
        mapped_stage,
        stage_default_probability,
        days_since_last_activity,
        incremental_acv,
        RENEWAL_ACV,
        WEIGHTED_IACV,
        DAYS_IN_STAGE
FROM base
WHERE MAPPED_STAGE NOT IN ('0-Pending Acceptance','1-Discovery','2-Scoping','3-Technical Evaluation', 'Duplicate', 'Unmapped', 'Unqualified')
AND [close_date:mtd]
ORDER BY 1 DESC, 2