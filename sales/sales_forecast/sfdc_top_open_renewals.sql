with opportunities as (

    SELECT * FROM analytics.sfdc_opportunity_xf

), final as (

    SELECT close_date::Date as close_date,
          opportunity_name,
          sales_type,
          OPPORTUNITY_OWNER,
          MAPPED_STAGE,
          incremental_acv
    FROM opportunities
    WHERE is_won = False
    AND STAGE_IS_CLOSED = False
    AND sales_type = 'Renewal'
    AND mapped_stage  NOT IN ('7-Closed', 'Unmapped', 'Unqualified', 'Duplicate')

)

SELECT * 
FROM final 
WHERE [close_date:mtd]
ORDER BY incremental_acv DESC