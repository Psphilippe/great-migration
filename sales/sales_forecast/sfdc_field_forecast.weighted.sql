with source as (

	SELECT * FROM analytics.sfdc_opportunity_xf

)

SELECT sum(forecasted_iacv)
FROM source
WHERE MAPPED_STAGE NOT IN ('0-Pending Acceptance', 'Duplicate', 
							'Unmapped', 'Unqualified')