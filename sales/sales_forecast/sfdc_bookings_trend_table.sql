with opportunities as (

    SELECT * FROM analytics.sfdc_opportunity_xf

), goals as (

    SELECT * FROM analytics.sheetload_iacv_goal

), agg_opportunities as (

    SELECT date_trunc('month', close_date)::date as close_month,
           sum(incremental_acv) as incremental_acv,
          count(distinct OPPORTUNITY_ID) as opportunities
    FROM opportunities
    WHERE is_won = True
    GROUP BY 1

), agg_goals as (

    SELECT date_day as month_of,
            sum(iacv_goal) as incremental_acv
    FROM goals
    GROUP BY 1

)

SELECT agg_opportunities.*, 
        agg_goals.incremental_acv as goal_iacv, 
        agg_opportunities.incremental_acv/goal_iacv as percent_to_goal
FROM agg_opportunities
LEFT JOIN agg_goals
ON agg_opportunities.close_month = agg_goals.month_of
ORDER BY 1 DESC
LIMIT 3
--WHERE [close_month:mtd]