with merge_requests as (
  SELECT merge_request_created_at,
         author_id,
         merge_request_id
  FROM analytics.gitlab_dotcom_merge_requests_xf
  WHERE is_community_contributor_related = TRUE
  AND merge_request_created_at > DATEADD('year', -1, CURRENT_TIMESTAMP())
  AND merge_request_state = 'merged'
)

SELECT DATE_TRUNC('month', merge_request_created_at) as merge_request_created_month,
       COUNT(distinct author_id) as author_distinct_count,
       COUNT(merge_request_id) as merge_request_count
FROM merge_requests
GROUP BY 1