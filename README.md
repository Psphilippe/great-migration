## How to Contribute

This project is organized with [The Great Migration Epic](https://gitlab.com/groups/gitlab-data/-/epics/11).

That epic has taken all the top-level spaces that used to be in other tools and borrows them for organizational purposes. 

Within each of those children Epics (most are functional groups), we have a collection of issues. These issues directly map to a dashboard or tile. 

Issue bodies should have three pieces of information: 
1. The existing (generated SQL)
1. The screenshot of the visualization
1. The CSV download

**If your screenshot or SQL contains customer names, make it confidential, otherwise default to public**

Dashboards are made of multiple tiles, so before taking action on it use the [quick action](https://docs.gitlab.com/ee/user/group/epics/#promoting-an-issue-to-an-epic) to promote it to an epic with each of the tiles in it as their own issues. Be sure that it does not lose the relationship to its parent issue or the parent's parent. If it does, be sure to correct it. 

After all the information is removed from Airtable for that unit (tile/dashboard/row) delete the row. (We have a secured copy elsewhere for reference.)

Once you have a created issue, create an MR for the issue. That MR will be used to save the rewritten SQL in a .sql file. Convert the stored (generated) SQL into alignment with the style guide (so on day 1, we are at 100% good to go) in the repo (as .sql) files. (This SQL should be inline with the style guide and numbers should be verified to match the screenshots. This is independent of our tool search.) The file should match the hierarchy of the epics and issues. Files have been created for top-level epics, but will need to be created for promoted epics (from dashboards).

## Next Steps

1. Work on SQL query naming conventions
1. Finalize updates to the SQL style guide

## What to look for while assessing tools
1. Check results from previous results (asymmetric aggs)
1. Assess what may need to be pushed into dbt.
1. Think about how we can have a MR-first workflow with the tool.
1. Think about formatting and parametrization.